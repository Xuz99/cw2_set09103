######################
#  START OF IMPORTS  #
######################

from flask import Flask, render_template, request, url_for, redirect, session
import sqlite3
import random
import string
from urlparse import urlparse

##################
#  START OF APP  #
##################

app = Flask(__name__)
app.secret_key = '5b&Sww3l,[t/{z2pKFv(Qav.C(Ih|<1b|?|;&$:g<Su39{D6Z6igW<Z<u9,U|3'

db = 'var/database.db'

#########################
#  START OF BASE INDEX  #
#########################

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/about_us')
def about_us():
  return render_template('about_us.html')


################################
#  START OF URL SHORTENER CODE #
################################

@app.route('/shorten_url', methods = ['POST','GET'])
def shorten_url():

  if 'user_id' in session:
	  user_id = session['user_id']
  else:
	  user_id = None

  if request.method == 'POST':

    try:
      get_full_url = request.form['url']
      short_url = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(5))

      parse_url = urlparse(get_full_url)
      if parse_url.scheme=='':
        full_url = 'http://'+parse_url.geturl()
      else:
        full_url = parse_url.geturl()

      with sqlite3.connect(db) as con:
        cursor = con.cursor()

        #: insert data into database
        cursor.execute("INSERT INTO urls(user_id,full_url,short_url) VALUES(?,?,?)",(user_id,full_url,short_url) )

        con.commit()


    except:
      con.rollback()

    finally:
      return render_template('shorten_url.html', short_url=short_url)
      con.close()

  else:
    return 'error'






@app.route('/<shorturl>')
def shorturl(shorturl):
  con = sqlite3.connect(db)
  con.row_factory = sqlite3.Row

  cursor = con.cursor()
  cursor.execute("SELECT * FROM urls WHERE short_url=?",(shorturl, ))

  rows = cursor.fetchall();

  for url in rows:
    get_url = url[1]
    return redirect(get_url)






###############################
#  START OF USER ACCOUNT CODE #
###############################

@app.route('/account')
def account():
    if 'user_id' in session:
        user_id = session['user_id']

        con = sqlite3.connect(db)
        con.row_factory = sqlite3.Row
        cursor = con.cursor()
        cursor.execute("SELECT rowid, * FROM urls WHERE user_id=?",(user_id, ))

        user_urls = cursor.fetchall()

        return render_template('account.html', user_urls = user_urls)
    else:
        return redirect(url_for('login'))

################################
#  START OF USER REGISTER CODE #
################################

@app.route('/register')
def register():
  return render_template('register.html')

@app.route('/register/user', methods = ['POST', 'GET'])
def register_user():

  if request.method == 'POST':

    try:
      username = request.form['username']
      password = request.form['password']

      #: connect to database
      with sqlite3.connect('var/database.db') as conn:
        c = conn.cursor()

        #: insert data into database
        c.execute("INSERT INTO users(username,password) VALUES(?,?)",(username,password))

        conn.commit()

    except:
      conn.rollback()

    finally:

      return redirect(url_for('login'))
      conn.close()





#############################
#  START OF USER LOGIN CODE #
#############################

@app.route('/login', methods = ['GET', 'POST'])
def login():
	if request.method == 'POST':
		username = request.form['username']
		password = request.form['password']
		conn = sqlite3.connect('var/database.db')
		c = conn.cursor()
		c.execute("SELECT rowid, * FROM users WHERE username=? AND password=?",(username,password))

		user = c.fetchall()
		if user:
			for row in user:
				session['user_id'] = row[0]
				session['username'] = row[1]
			return redirect(url_for('index'))
		else:
			return redirect(url_for('login'))
		conn.close()
	return render_template('login.html')






##############################
#  START OF USER LOGOUT CODE #
##############################

@app.route('/logout')
def logout():
  session.pop('username', None)
  session.pop('user_id', None)
  return redirect(url_for('login'))





########################
#  START OF ERROR CODE #
########################

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(400)
def key_error(e):
    return render_template('400.html'), 400

@app.errorhandler(500)
def internal_server_error(e):
    return render_template('index.html'), 500





################
#  END OF APP  #
################

if __name__ == "__main__":
  app.run(host='0.0.0.0', debug=True)
