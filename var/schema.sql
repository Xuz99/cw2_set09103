CREATE TABLE urls (
	user_id TEXT,
	full_url TEXT,
	short_url TEXT
);

CREATE TABLE users (
	username TEXT UNIQUE,
	password TEXT
);
